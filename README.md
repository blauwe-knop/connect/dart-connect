An ECDSA, ECIES and AES implementation in Dart.

This package currently only supports the elliptic curve known as `secp256r1` or P256, and uses AES CTR.

## Usage

### Encryption with ECIES

```dart
final message = "Hello, world!";

final KeyPair keyPair = Ec.generateKeyPair();

final cipherText = Ecies.encrypt(keyPair.publicKey, utf8.encode(message));

final plainText = utf8.decode(Ecies.decrypt(keyPair.privateKey, cipherText));

assert(message == plainText);
```

### Signing with ECDSA

```dart
final message = utf8.encode('Hello, world!');

final KeyPair keyPair = Ec.generateKeyPair();

final signature = Ecdsa.sign(keyPair.privateKey, message);  

assert(Ecdsa.verify(keyPair.publicKey, signature, message));
```

### Encryption with AES

```dart
  final message = "Hello, world!";

  final key = Aes.generateKey();

  final cipherText = Aes.encrypt(key, utf8.encode(message));  

  final plainText = utf8.decode(Aes.decrypt(key, cipherText));  

  assert(message == plainText);
```

### Importing and exporting keys

Existing private and public keys may also be imported and exported to/from DER and PEM using the [`basic_utils`](https://pub.dev/packages/basic_utils) package, with the following functions:
- `CryptoUtils.ecPublicKeyFromPem()`
- `CryptoUtils.ecPrivateKeyFromPem()`
- `CryptoUtils.encodeEcPublicKeyToPem()`
- `CryptoUtils.encodeEcPrivateKeyToPem()`
- `CryptoUtils.ecPublicKeyFromDerBytes()`
- `CryptoUtils.ecPrivateKeyFromDerBytes()`

This package does not seem to have functions for encoding keys to DER format.
For that, the corresponding PEM functions in conjunction with `CryptoUtils.getBytesFromPEMString()` can be used.
