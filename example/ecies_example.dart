// ignore_for_file: deprecated_member_use_from_same_package

import 'dart:convert';
import 'package:dart_connect/dart_connect.dart';

void main() {
  final message = "Hello, world!";

  final KeyPair keyPair = Ec.generateKeyPair(curve: Curve.prime256v1);

  final cipherText = Ecies.encrypt(keyPair.publicKey, utf8.encode(message));
  print(cipherText);

  final plainText = utf8.decode(Ecies.decrypt(keyPair.privateKey, cipherText));
  print(plainText);

  assert(message == plainText);
}
