import 'dart:convert';
import 'package:dart_connect/dart_connect.dart';

Future<void> main() async {
  // Encode the JSON body
  final jsonBody = json.encode({
    "sub": "sub",
    "iss": "iss",
    "configuration_token": "configuration_token",
  });

  final keyPair = Ec.generateKeyPair(curve: Curve.prime256v1);

  final headers = Jwe.createEciesHeader;

  final encryptedJwe =
      await Jwe.encryptEcies(keyPair.publicKey, jsonBody, headers);
  print('Encrypted JWE: $encryptedJwe');

  final decryptedPayload =
      await Jwe.decryptEcies(encryptedJwe, keyPair.privateKey);
  print('Decrypted Claims: ${decryptedPayload.payload}');

  assert(jsonBody == decryptedPayload.payload);
}
