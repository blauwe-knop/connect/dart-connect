import 'dart:convert';
import 'package:dart_connect/dart_connect.dart';

Future<void> main() async {
  // Encode the JSON body
  final jsonBody = json.encode({
    "sub": "sub",
    "iss": "iss",
    "configuration_token": "configuration_token",
  });

  final key = Aes.generateKey();

  final headers = Jwe.createAesHeader;

  final encryptedJwe = await Jwe.encryptAes(key, jsonBody, headers);
  print('Encrypted JWE: $encryptedJwe');

  final decryptedPayload = await Jwe.decryptAes(encryptedJwe, key);
  print('Decrypted Claims: ${decryptedPayload.payload}');

  assert(jsonBody == decryptedPayload.payload);
}
