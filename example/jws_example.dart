import 'dart:convert';
import 'package:dart_connect/dart_connect.dart';

Future<void> main() async {
  final KeyPair keyPair = Ec.generateKeyPair(curve: Curve.prime256v1);

  Future<String?> customSignFunction(String signInput) async {
    final signature = Ecdsa.sign(keyPair.privateKey, utf8.encode(signInput));

    return base64Url.encode(signature);
  }

  Future<bool?> customVerifyFunction({
    required String signature,
    required String message,
  }) async {
    final decodedSignature = base64Url.decode(signature);

    return Ecdsa.verify(
        keyPair.publicKey, decodedSignature, utf8.encode(message));
  }

  final payload = json.encode({
    'sub': 'document',
    'document': "FINANCIAL_CLAIMS_INFORMATION_REQUEST",
  });

  final headers = Jws.createHeader;

  final jws = await Jws.sign(payload, headers, customSignFunction);
  print('Signed JWS: $jws');

  final jwsDocument = await Jws.verify(
    signedJws: jws,
    verifyFunction: customVerifyFunction,
  );
  print('Verified JWS Payload: ${jwsDocument.payload}');
}
