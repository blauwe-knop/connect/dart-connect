import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';

void main() {
  final message = "Hello, world!";

  final key = Aes.generateKey();

  final encryptedData = Aes.encrypt(key, utf8.encode(message));
  print('Encrypted: $encryptedData');

  final plainText = utf8.decode(Aes.decrypt(key, encryptedData));
  print('Decrypted: $plainText');

  assert(message == plainText);
}
