// ignore_for_file: deprecated_member_use_from_same_package

import 'dart:convert';
import 'package:dart_connect/dart_connect.dart';

void main() {
  final message = utf8.encode('Hello, world!');

  final KeyPair keyPair = Ec.generateKeyPair(curve: Curve.prime256v1);

  final signature = Ecdsa.sign(keyPair.privateKey, message);
  print(signature);

  assert(Ecdsa.verify(keyPair.publicKey, signature, message));
}
