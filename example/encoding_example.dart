import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';

void main() {
  final message = "Hello, world!";
  print(message);

  final utf8EncodedMessage = utf8.encode(message);
  print(utf8EncodedMessage);

  final encodedMessage = Base64.parseFromBytes(utf8EncodedMessage);
  print(encodedMessage);

  final utf8EndocedPlainText = Base64.toBytes(encodedMessage);
  print(utf8EndocedPlainText);

  final plainText = utf8.decode(utf8EndocedPlainText);
  print(plainText);

  assert(message == plainText);
}
