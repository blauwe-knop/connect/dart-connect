import 'dart:convert';
import 'package:dart_connect/dart_connect.dart';
import 'package:test/test.dart';

final String publicKeyPem = '''-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80
AoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END PUBLIC KEY-----''';

final String privateKeyPem = '''-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----''';

void main() {
  group('AES tests', () {
    test('Generates a valid AES key', () {
      final key = Aes.generateKey();
      expect(key.length, equals(Aes.aesKeySize));
    });

    test('AES GCM encryption and decryption works correctly', () async {
      // Generate a random plaintext
      final plainText = utf8.encode("Hello world");
      final key = Aes.generateKey();

      // Encrypt the plaintext
      final encryptedData = Aes.encrypt(key, plainText);

      final decryptedPlainText = Aes.decrypt(key, encryptedData);

      expect(decryptedPlainText, equals(plainText));
    });

    test('AES GCM encryption and decryption with Go generated AES key',
        () async {
      // Generate a random plaintext
      final plainText = utf8.encode("Hello world");
      final key = Base64.toBytes(("ol8Katbs0EOuvrJnnT7j3A=="));

      // Encrypt the plaintext
      final encryptedData = Aes.encrypt(key, plainText);

      final decryptedPlainText = Aes.decrypt(key, encryptedData);

      expect(decryptedPlainText, equals(plainText));
    });
  });
}
