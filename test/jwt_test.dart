import 'dart:convert';
import 'dart:typed_data';
import 'package:dart_connect/src/crypto/aes.dart';
import 'package:dart_connect/src/crypto/curve.dart';
import 'package:dart_connect/src/crypto/ec.dart';
import 'package:dart_connect/src/jwt/jwe.dart';
import 'package:dart_connect/src/jwt/jws.dart';
import 'package:dart_connect/src/jwt/jwk.dart';
import 'package:test/test.dart';

void main() {
  group('Jws', () {
    test('should create JWS with default headers', () {
      final headers = Jws.createHeader;

      expect(headers['typ'], 'JWT');
      expect(headers['alg'], 'ES256');
    });

    test('should sign JWS', () async {
      final payload = json.encode({'sub': 'document', 'document': 'test'});
      final headers = Jws.createHeader;

      Future<String?> signFunction(String signInput) async {
        return 'signature';
      }

      final jws = await Jws.sign(payload, headers, signFunction);
      expect(jws, contains('signature'));
    });

    test('should throw exception if signature is null', () async {
      final payload = json.encode({'sub': 'document', 'document': 'test'});
      final headers = Jws.createHeader;

      Future<String?> signFunction(String signInput) async {
        return null;
      }

      expect(() async => await Jws.sign(payload, headers, signFunction),
          throwsA(isA<JwsSignException>()));
    });

    test('should verify JWS', () async {
      final payload = json.encode({'sub': 'document', 'document': 'test'});
      final headers = Jws.createHeader;

      Future<String?> signFunction(String signInput) async {
        return 'signature';
      }

      Future<bool?> verifyFunction({
        required String signature,
        required String message,
      }) async {
        return signature == 'signature';
      }

      final jws = await Jws.sign(payload, headers, signFunction);

      final jwsDocument = await Jws.verify(
        signedJws: jws,
        verifyFunction: verifyFunction,
      );

      expect(jwsDocument.payload, payload);
    });
  });

  group('Jwe aes', () {
    late Uint8List aesKey;

    setUp(() async {
      aesKey = Aes.generateKey();
    });

    test('should create JWE with AES headers', () {
      final jweHeader = Jwe.createAesHeader;

      expect(jweHeader['alg'], 'dir');
      expect(jweHeader['enc'], 'A256GCM');
    });

    test('JWE encryption and decryption work correctly with AES', () async {
      final payload = json.encode({
        "sub": "sub",
        "iss": "iss",
        "configuration_token": "configuration_token",
      });

      final header = Jwe.createAesHeader;

      final jwe = await Jwe.encryptAes(aesKey, payload, header);
      final jweDocument = await Jwe.decryptAes(jwe, aesKey);

      expect(jweDocument.payload, payload);
    });

    test('should throw exception if JWE encrypt fails', () async {
      final payload = 'test';
      final header = Jwe.createAesHeader;

      expect(
          () async =>
              await Jwe.encryptAes(utf8.encode("invalid key"), payload, header),
          throwsA(isA<JweEncryptException>()));
    });

    test('should throw exception if JWE format is invalid', () async {
      final invalidJwe = '0.1.2.3';

      expect(
          () async => await Jwe.decryptAes(invalidJwe, aesKey),
          throwsA(predicate((e) =>
              e is JweDecryptException &&
              e.message.contains('Invalid JWE format'))));
    });

    test('should sign JWS, encrypt JWS as JWE, decrypt JWE and verify JWS',
        () async {
      final payload = json.encode({'sub': 'document', 'document': 'test'});
      final jwsHeaders = Jws.createHeader;

      Future<String?> signFunction(String signInput) async {
        return 'signature';
      }

      final jws = await Jws.sign(payload, jwsHeaders, signFunction);

      final jweHeaders = Jwe.createAesHeader;

      final encryptedJwe = await Jwe.encryptAes(aesKey, jws, jweHeaders);
      final decryptedJws = await Jwe.decryptAes(encryptedJwe, aesKey);

      expect(decryptedJws.payload, jws);
    });
  });

  group('Jwe ecies', () {
    late KeyPair keyPair;

    setUp(() async {
      keyPair = Ec.generateKeyPair(curve: Curve.prime256v1);
    });

    test('should create JWE with ECIES headers', () {
      final headers = Jwe.createEciesHeader;

      expect(headers['alg'], 'ECDH-ES');
      expect(headers['enc'], 'A256GCM');
    });

    test('JWE encryption and decryption work correctly with ECIES', () async {
      final payload = json.encode({
        "sub": "sub",
        "iss": "iss",
        "configuration_token": "configuration_token",
      });

      final headers = Jwe.createEciesHeader;

      final encryptedJwe =
          await Jwe.encryptEcies(keyPair.publicKey, payload, headers);
      final decryptedJwe =
          await Jwe.decryptEcies(encryptedJwe, keyPair.privateKey);

      expect(decryptedJwe.payload, payload);
    });

    test('should throw exception if JWE format is invalid', () async {
      final invalidJwe = '0.1.2.3';

      expect(
          () async => await Jwe.decryptEcies(invalidJwe, keyPair.privateKey),
          throwsA(predicate((e) =>
              e is JweDecryptException &&
              e.message.contains('Invalid JWE format'))));
    });

    test('should sign JWS, encrypt JWS as JWE, decrypt JWE and verify JWS',
        () async {
      final payload = json.encode({'sub': 'document', 'document': 'test'});
      final jwsHeaders = Jws.createHeader;

      Future<String?> signFunction(String signInput) async {
        return 'signature';
      }

      final jws = await Jws.sign(payload, jwsHeaders, signFunction);

      final jweHeaders = Jwe.createEciesHeader;

      final encryptedJwe =
          await Jwe.encryptEcies(keyPair.publicKey, jws, jweHeaders);

      final decryptedJws =
          await Jwe.decryptEcies(encryptedJwe, keyPair.privateKey);

      expect(decryptedJws.payload, jws);
    });
  });

  group('Jwk', () {
    test('should convert ephemeral public key to JWK and back', () {
      final publicKey = Ec.generateKeyPair(curve: Curve.prime256v1).publicKey;
      final ecPublicKey = ecPublicKeyFromPublicKey(publicKey);
      final jwk = Jwk.ecPublicKeyToJwk(ecPublicKey);
      final convertedKey = Jwk.jwkToEcPublicKey(jwk);

      expect(ecPublicKey.Q, convertedKey.Q);
    });

    test('should throw exception if JWK is invalid', () {
      final invalidJwk = {'kty': 'invalid'};

      expect(
          () => Jwk.jwkToEcPublicKey(invalidJwk), throwsA(isA<JwkException>()));
    });
  });
}
