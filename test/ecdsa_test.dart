// ignore_for_file: deprecated_member_use_from_same_package

import 'dart:convert';
import 'dart:typed_data';
import 'package:basic_utils/basic_utils.dart' as utils;
import 'package:dart_connect/src/crypto/curve.dart';
import 'package:dart_connect/src/crypto/ecdsa.dart';
import 'package:dart_connect/src/crypto/ec.dart';
import 'package:test/test.dart';

final String publicKeyPem = '''-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80
AoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END PUBLIC KEY-----''';

final String privateKeyPem = '''-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----''';

void main() {
  group('ECDSA tests', () {
    test('sign and verify with hardcoded keys', () {
      final message = utf8.encode('hello');
      final privateKey = Ec.parsePrivateKeyFromPem(utf8.encode(privateKeyPem));
      final publicKey = Ec.parsePublicKeyFromPem(utf8.encode(publicKeyPem));
      final signature = Ecdsa.sign(privateKey, message);
      expect(Ecdsa.verify(publicKey, signature, message), true);
    });
    test('sign and verify with random keys', () {
      final KeyPair keyPair = Ec.generateKeyPair(curve: Curve.prime256v1);
      final message = utf8.encode('hello');
      final signature = Ecdsa.sign(keyPair.privateKey, message);
      expect(Ecdsa.verify(keyPair.publicKey, signature, message), true);
    });

    test('parse custom public key object from pem', () {
      final encodedPublicKeyPem = utf8.encode(publicKeyPem);
      final PublicKey publicKey = Ec.parsePublicKeyFromPem(encodedPublicKeyPem);

      final utils.ECPublicKey ecPublicKey =
          utils.CryptoUtils.ecPublicKeyFromPem(publicKeyPem);

      expect(publicKey.x == ecPublicKey.Q?.x?.toBigInteger(), true);
      expect(publicKey.y == ecPublicKey.Q?.y?.toBigInteger(), true);
      expect(publicKey.curve == Curve.prime256v1.value, true);
    });
    test('parse custom private key object from pem', () {
      final privateKeyPemBytes = utf8.encode(privateKeyPem);
      final PrivateKey privateKey =
          Ec.parsePrivateKeyFromPem(privateKeyPemBytes);

      final utils.ECPrivateKey ecPrivateKey =
          utils.CryptoUtils.ecPrivateKeyFromPem(privateKeyPem);

      expect(privateKey.d == ecPrivateKey.d, true);
      expect(privateKey.curve == Curve.prime256v1.value, true);
    });

    test('verify a message signed in Go', () {
      final message = utf8.encode('hello');

      final Uint8List signature = Uint8List.fromList([
        48,
        70,
        2,
        33,
        0,
        159,
        125,
        171,
        120,
        245,
        35,
        21,
        216,
        163,
        79,
        243,
        17,
        54,
        230,
        224,
        236,
        226,
        168,
        68,
        62,
        155,
        212,
        75,
        226,
        36,
        15,
        223,
        19,
        37,
        5,
        15,
        112,
        2,
        33,
        0,
        158,
        163,
        213,
        240,
        43,
        108,
        24,
        159,
        36,
        187,
        128,
        95,
        29,
        58,
        126,
        48,
        184,
        199,
        2,
        169,
        168,
        221,
        142,
        25,
        201,
        28,
        82,
        177,
        124,
        21,
        155,
        108
      ]);
      final publicKey = Ec.parsePublicKeyFromPem(utf8.encode(publicKeyPem));
      expect(Ecdsa.verify(publicKey, signature, message), true);
    });
  });
}
