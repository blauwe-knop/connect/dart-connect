// ignore_for_file: deprecated_member_use_from_same_package

import 'dart:convert';
import 'dart:typed_data';
import 'package:convert/convert.dart';
import 'package:dart_connect/dart_connect.dart';
import 'package:test/test.dart';

final String publicKeyPem = '''-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80
AoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END PUBLIC KEY-----''';

final String privateKeyPem = '''-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----''';

void main() {
  group('ECIES tests', () {
    test('encryption with hardcoded keys', () {
      encryptAndDecrypt(
        Ec.parsePublicKeyFromPem(utf8.encode(publicKeyPem)),
        Ec.parsePrivateKeyFromPem(utf8.encode(privateKeyPem)),
      );
    });

    test('encryption with random keys', () {
      final KeyPair keyPair = Ec.generateKeyPair(curve: Curve.prime256v1);

      encryptAndDecrypt(keyPair.publicKey, keyPair.privateKey);
    });

    test('decrypt a ciphertext that was encrypted in Go', () {
      final ciphertext =
          'BMG0gADC1w3o4/Blrtr1fjgBotB4mO3eCcWT8UStlmXFQ+KicfsGGbTPz7BQcCyRE3XPT1nu7wTRMdymhJTGMgG6QSwNqmYR7jkE6uNqXSOyiGQDYdxVd0r9Z8FA2d03f8rXQUxra0aSevQJoFzUwPlUbmKQ/j4fvZwqkBxiBbxL1fgunG0/BN4ilnLhLGmMT4OCW/vgH4++';
      final cipherTextBytes = Base64.toBytes(ciphertext);
      final PrivateKey privateKey =
          Ec.parsePrivateKeyFromPem(utf8.encode(privateKeyPem));
      final plaintext = utf8.decode(Ecies.decrypt(privateKey, cipherTextBytes));
      expect(plaintext, 'Another messsage to encrypt and decrypt.');
    });

    test('decrypt a ciphertext that was encrypted in Go with shared info', () {
      final ciphertext =
          'BDmqgJucV+B549kRScymu+Ozcb8JKI7J42LIt1HXGuoLsMYUwCZ14ua3TVb2aMjNNODhjOu4VUA26NL8mx5+J8fwpFGLW2q5xvikyg1sWQRhkRNsOHCwdUnMy6gu/iKmyF6KChf+M8U9FJigiXCMp4UlQZtAOUGZIn0sDVmEd/CvbqVFOjqFL7H67mzBpL9uhO6jyLWSncqo';
      final cipherTextBytes = Base64.toBytes(ciphertext);
      final PrivateKey privateKey =
          Ec.parsePrivateKeyFromPem(utf8.encode(privateKeyPem));
      final plaintext = utf8.decode(Ecies.decrypt(privateKey, cipherTextBytes,
          sharedInfo1: Uint8List.fromList('s1'.codeUnits),
          sharedInfo2: Uint8List.fromList('s2'.codeUnits)));
      expect(plaintext, 'Another messsage to encrypt and decrypt.');
    });
  });

  test('ConcatKDF test', () {
    // Test case taken from https://github.com/ethereum/go-ethereum/blob/master/crypto/ecies/ecies_test.go#L51
    final output =
        Ecies.concatKDF(Uint8List.fromList('input'.codeUnits), 32, null);
    expect(hex.encode(output),
        '858b192fa2ed4395e2bf88dd8d5770d67dc284ee539f12da8bceaa45d06ebae0');
  });

  test('constant-time list comparison', () {
    expect(
        Ecies.constTimeCompare(
            Uint8List.fromList([1, 2]), Uint8List.fromList([1, 2])),
        true);

    expect(
        Ecies.constTimeCompare(
            Uint8List.fromList([1, 2]), Uint8List.fromList([2, 1])),
        false);
    expect(
        Ecies.constTimeCompare(
            Uint8List.fromList([1, 2]), Uint8List.fromList([1, 3])),
        false);
    expect(
        Ecies.constTimeCompare(
            Uint8List.fromList([1, 2]), Uint8List.fromList([3, 4])),
        false);

    expect(
        Ecies.constTimeCompare(
            Uint8List.fromList([1, 2]), Uint8List.fromList([1])),
        false);
    expect(
        Ecies.constTimeCompare(
            Uint8List.fromList([1, 2]), Uint8List.fromList([1, 2, 3])),
        false);
  });
}

void encryptAndDecrypt(PublicKey publicKey, PrivateKey privateKey) {
  final message = 'hello';

  final ciphertext = Ecies.encrypt(publicKey, utf8.encode(message));
  print(ciphertext);

  final decrypted = utf8.decode(Ecies.decrypt(privateKey, ciphertext));
  print(decrypted);

  expect(message, decrypted);
}
