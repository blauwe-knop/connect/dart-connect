import 'dart:convert';
import 'dart:typed_data';

import 'package:dart_connect/src/crypto/aes.dart';
import 'package:dart_connect/src/crypto/ec.dart';
import 'package:dart_connect/src/crypto/ecies.dart';
import 'package:dart_connect/src/jwt/jwk.dart';

// Represents a JSON Web Encryption (JWE).
class Jwe {
  /// Creates a JWE header for Aes encryption.
  static JweHeader get createAesHeader {
    return {
      'alg': 'dir',
      'enc': 'A256GCM',
    };
  }

  /// Creates a JWE header for ECIES encryption.
  static JweHeader get createEciesHeader {
    return {
      'alg': 'ECDH-ES',
      'enc': 'A256GCM',
    };
  }

  /// Encrypts the JWE payload using AES.
  ///
  /// Returns the encrypted JWE as a string.
  /// Throws a [JweEncryptException] if encryption fails.
  static Future<String> encryptAes(
      Uint8List key, JwePayload payload, JweHeader headers) async {
    try {
      final encryptedData = Aes.encrypt(key, utf8.encode(payload));
      final (iv, ciphertext, tag) = Aes.parseInput(encryptedData);

      final base64UrlEncodedHeaders =
          base64Url.encode(utf8.encode(jsonEncode(headers)));

      final encodedEncryptedciphertext = base64Url.encode(ciphertext);
      final encodedEncryptedIv = base64Url.encode(iv);
      final encodedEncryptedTag = base64Url.encode(tag);
      final encodedEncryptedKey = base64Url.encode(utf8.encode(''));

      return '$base64UrlEncodedHeaders.$encodedEncryptedKey.$encodedEncryptedIv.$encodedEncryptedciphertext.$encodedEncryptedTag';
    } catch (ex) {
      throw JweEncryptException('Failed to encrypt document jws: $ex');
    }
  }

  /// Encrypts the JWE payload using ECIES.
  ///
  /// Returns the encrypted JWE as a string.
  /// Throws a [JweEncryptException] if encryption fails.
  static Future<String> encryptEcies(
      PublicKey publicKey, JwePayload payload, JweHeader headers) async {
    try {
      final encryptedData = Ecies.encrypt(publicKey, utf8.encode(payload));
      final (ecPublicKey, iv, ciphertext, tag) =
          Ecies.parseInput(encryptedData);

      headers['epk'] = Jwk.ecPublicKeyToJwk(ecPublicKey);

      final base64UrlEncodedHeaders =
          base64Url.encode(utf8.encode(jsonEncode(headers)));

      final encodedEncryptedciphertext = base64Url.encode(ciphertext);
      final encodedEncryptedIv = base64Url.encode(iv);
      final encodedEncryptedTag = base64Url.encode(tag);
      final encodedEncryptedKey = base64Url.encode(utf8.encode(''));

      return '$base64UrlEncodedHeaders.$encodedEncryptedKey.$encodedEncryptedIv.$encodedEncryptedciphertext.$encodedEncryptedTag';
    } catch (ex) {
      throw JweEncryptException('Failed to encrypt document jws: $ex');
    }
  }

  /// Decrypts the JWE ciphertext using AES.
  ///
  /// Throws a [JweDecryptException] if decryption fails.
  static Future<JweDocument> decryptAes(String jweString, Uint8List key) async {
    final jweParts = jweString.split('.');

    if (jweParts.length != 5) {
      throw JweDecryptException('Invalid JWE format');
    }

    try {
      final header = json.decode(utf8.decode(base64Url.decode(jweParts[0])));
      final iv = base64Url.decode(jweParts[2]);
      final encryptedCiphertext = base64Url.decode(jweParts[3]);
      final tag = base64Url.decode(jweParts[4]);

      Uint8List encryptedData = Aes.combineOutput(iv, encryptedCiphertext, tag);
      final ciphertext = Aes.decrypt(key, encryptedData);

      return JweDocument(
        header: header,
        payload: utf8.decode(ciphertext),
      );
    } catch (ex) {
      throw JweDecryptException('Failed to decrypt document jws: $ex');
    }
  }

  /// Decrypts the JWE ciphertext using ECIES.
  ///
  /// Throws a [JweDecryptException] if decryption fails.
  static Future<JweDocument> decryptEcies(
      String jweString, PrivateKey privateKey) async {
    final jweParts = jweString.split('.');

    if (jweParts.length != 5) {
      throw JweDecryptException('Invalid JWE format');
    }

    try {
      final header = json.decode(utf8.decode(base64Url.decode(jweParts[0])));
      final iv = base64Url.decode(jweParts[2]);
      final encryptedCiphertext = base64Url.decode(jweParts[3]);
      final tag = base64Url.decode(jweParts[4]);

      final ecPublicKey = Jwk.jwkToEcPublicKey(header['epk']);

      final encryptedData =
          Ecies.combineOutput(ecPublicKey, iv, encryptedCiphertext, tag);

      final ciphertext = Ecies.decrypt(privateKey, encryptedData);

      return JweDocument(header: header, payload: utf8.decode(ciphertext));
    } catch (ex) {
      throw JweDecryptException('Failed to decrypt document jws: $ex');
    }
  }
}

typedef JweHeader = Map<String, dynamic>;
typedef JwePayload = String;

/// Represents a JWE Document.
class JweDocument {
  final JweHeader header;
  final JwePayload payload;

  JweDocument({
    required this.header,
    required this.payload,
  });
}

/// Exception thrown when JWE encryption fails.
class JweEncryptException implements Exception {
  final String message;

  JweEncryptException(this.message);

  @override
  String toString() => 'JweEncryptException: $message';
}

/// Exception thrown when JWE decryption fails.
class JweDecryptException implements Exception {
  final String message;

  JweDecryptException(this.message);

  @override
  String toString() => 'JweDecryptException: $message';
}
