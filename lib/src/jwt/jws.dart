import 'dart:convert';

/// Represents a JSON Web Signature (JWS).
class Jws {
  /// Creates a JWS header with default values.
  static JwsHeader get createHeader {
    return {
      "typ": "JWT",
      "alg": "ES256",
    };
  }

  /// Signs the JWS using the provided [signFunction].
  ///
  /// Returns the signed JWS as a string.
  /// Throws a [JwsSignException] if the signature is null.
  static Future<String> sign(JwsPayload payload, JwsHeader headers,
      JwsSignFunction signFunction) async {
    final base64EncodedHeaders =
        base64Url.encode(utf8.encode(jsonEncode(headers)));
    final base64EncodedPayload = base64Url.encode(utf8.encode(payload));

    final jwsSigningInput = '$base64EncodedHeaders.$base64EncodedPayload';
    final encodedJwsSignature = await signFunction(jwsSigningInput);

    if (encodedJwsSignature == null) {
      throw JwsSignException('Encoded document jws signature cannot be null');
    }

    final signedJws =
        '$base64EncodedHeaders.$base64EncodedPayload.$encodedJwsSignature';

    return signedJws;
  }

  /// Verifies the JWS using the provided [verifyFunction].
  ///
  /// Throws a [JwsVerifyException] if the verification fails.
  static Future<JwsDocument> verify({
    required String signedJws,
    required JwsVerifyFunction verifyFunction,
  }) async {
    final jwsParts = signedJws.split('.');

    if (jwsParts.length != 3) {
      throw JwsVerifyException('Invalid JWS format, expected 3 parts but got '
          '${jwsParts.length}');
    }

    final signature = jwsParts[2];
    final signedJwsInput = '${jwsParts[0]}.${jwsParts[1]}';
    final verified = await verifyFunction(
      signature: signature,
      message: signedJwsInput,
    );

    if (verified == null || !verified) {
      throw JwsVerifyException('Failed to verify document jws');
    }

    final header = json.decode(utf8.decode(base64Url.decode(jwsParts[0])));
    final payload = utf8.decode(base64Url.decode(jwsParts[1]));

    return JwsDocument(
      header: header,
      payload: payload,
      signature: signature,
    );
  }
}

// Custom sign function type definition
typedef JwsSignFunction = Future<JwsSignature?> Function(String signInput);
typedef JwsVerifyFunction = Future<bool?> Function({
  required String signature,
  required String message,
});

/// Represents a JWS Document.
///
/// Creates a new [JwsDocument] instance.
class JwsDocument {
  final JwsHeader header;
  final JwsPayload payload;
  final JwsSignature signature;

  JwsDocument({
    required this.header,
    required this.payload,
    required this.signature,
  });
}

typedef JwsHeader = Map<String, dynamic>;
typedef JwsPayload = String;
typedef JwsSignature = String;

/// Exception thrown when JWS signing fails.
class JwsSignException implements Exception {
  /// The exception message.
  final String message;

  /// Creates a new [JwsSignException] instance.
  JwsSignException(this.message);

  @override
  String toString() => 'JwsSignException: $message';
}

/// Exception thrown when JWS verification fails.
class JwsVerifyException implements Exception {
  /// The exception message.
  final String message;

  /// Creates a new [JwsVerifyException] instance.
  JwsVerifyException(this.message);

  @override
  String toString() => 'JwsVerifyException: $message';
}
