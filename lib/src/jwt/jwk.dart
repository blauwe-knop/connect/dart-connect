import 'dart:convert';

import 'package:basic_utils/basic_utils.dart' as utils;
import 'package:dart_connect/src/crypto/ecies.dart';
import 'package:pointycastle/ecc/curves/secp256r1.dart';

class Jwk {
  static Map<String, dynamic> ecPublicKeyToJwk(utils.ECPublicKey publicKey) {
    try {
      final x = base64Url.encode(publicKey.Q!.x!.toBigInteger()!.toBytes());
      final y = base64Url.encode(publicKey.Q!.y!.toBigInteger()!.toBytes());

      return {
        "kty": "EC",
        "crv": "P-256",
        "x": x,
        "y": y,
      };
    } catch (ex) {
      throw JwkException('Failed to convert ephemeral public key to JWK: $ex');
    }
  }

  static utils.ECPublicKey jwkToEcPublicKey(Map<String, dynamic> jwk) {
    try {
      final x = BigInt.parse(
          base64Url
              .decode(jwk['x']!)
              .map((e) => e.toRadixString(16).padLeft(2, '0'))
              .join(),
          radix: 16);

      final y = BigInt.parse(
          base64Url
              .decode(jwk['y']!)
              .map((e) => e.toRadixString(16).padLeft(2, '0'))
              .join(),
          radix: 16);

      final curve = ECCurve_secp256r1();

      final ecPoint = curve.curve.createPoint(x, y);

      return utils.ECPublicKey(ecPoint, ECCurve_secp256r1());
    } catch (ex) {
      throw JwkException('Failed to convert JWK to ephemeral public key: $ex');
    }
  }
}

class JwkException implements Exception {
  final String message;

  JwkException(this.message);

  @override
  String toString() => 'JwkException: $message';
}
