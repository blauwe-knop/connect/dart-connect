import 'dart:convert';
import 'dart:typed_data';

typedef Base64String = String;

class Base64 {
  static Uint8List toBytes(Base64String message) {
    return base64Decode(message);
  }

  static Base64String parseFromBytes(Uint8List bytes) {
    return base64Encode(bytes);
  }
}
