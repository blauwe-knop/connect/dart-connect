import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart' as utils;
import 'package:dart_connect/src/crypto/algorithm_names.dart';
import 'package:dart_connect/src/crypto/secure_random.dart';
import 'package:dart_connect/src/crypto/ec.dart';
import 'package:pointycastle/asn1.dart';

class Ecdsa {
  @Deprecated("Sign will no longer use a privatekey")
  static Uint8List sign(PrivateKey privateKey, Uint8List message) {
    final utils.ECPrivateKey ecPrivateKey =
        ecPrivateKeyFromPrivateKey(privateKey);

    final signer = utils.Signer(AlgorithmNames.sha256ecdsa.value)
      ..init(
          true,
          utils.ParametersWithRandom(
              utils.PrivateKeyParameter(ecPrivateKey), newSecureRandom()));
    final signature = signer.generateSignature(message) as utils.ECSignature;

    return ASN1Sequence(
            elements: [ASN1Integer(signature.r), ASN1Integer(signature.s)])
        .encode();
  }

  static bool verify(
      PublicKey publicKey, Uint8List signature, Uint8List message) {
    // Parse signature
    utils.ECSignature ecdsaSig;
    try {
      final seq = ASN1Parser(signature).nextObject() as ASN1Sequence;
      if (seq.elements?.length != 2) {
        throw Exception('expected two ASN1 elements');
      }
      ecdsaSig = utils.ECSignature((seq.elements?[0] as ASN1Integer).integer!,
          (seq.elements?[1] as ASN1Integer).integer!);
    } catch (ex) {
      throw Exception('invalid signature: $ex');
    }

    // Verify signature
    final utils.ECPublicKey ecPublicKey = ecPublicKeyFromPublicKey(publicKey);
    final verifier = utils.Signer(AlgorithmNames.sha256ecdsa.value)
      ..init(false, utils.PublicKeyParameter(ecPublicKey));
    return verifier.verifySignature(message, ecdsaSig);
  }
}
