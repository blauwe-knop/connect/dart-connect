import 'dart:typed_data';
import 'package:dart_connect/src/crypto/secure_random.dart';
import "package:pointycastle/export.dart";

class Aes {
  static const int macSize = 128; // 128 bits
  static const int aesKeySize = 16; // 16 bytes
  static const int nonceSize = 12; // 12 bytes

  static Uint8List generateKey() => newSecureRandom().nextBytes(aesKeySize);

  static Uint8List encrypt(Uint8List key, Uint8List plainText) {
    final cipher = GCMBlockCipher(AESEngine())
      ..init(
        true,
        AEADParameters(
          KeyParameter(key),
          macSize,
          Uint8List(nonceSize),
          Uint8List(0),
        ),
      );

    final nonce = cipher.nonce;

    final ciphertext = cipher.process(plainText);

    final output = Uint8List(nonce.length + ciphertext.length);
    output.setRange(0, nonce.length, nonce);
    output.setRange(nonce.length, output.length, ciphertext);

    return output;
  }

  static Uint8List decrypt(Uint8List key, Uint8List cipherText) {
    final nonce = Uint8List(nonceSize);
    final cipherTextWithTag = Uint8List(cipherText.length - nonce.length);
    nonce.setRange(0, nonce.length, cipherText);
    cipherTextWithTag.setRange(
        0, cipherTextWithTag.length, cipherText, nonce.length);

    final cipher = GCMBlockCipher(AESEngine())
      ..init(
        false,
        AEADParameters(
          KeyParameter(key),
          macSize,
          nonce,
          Uint8List(0),
        ),
      );

    final plaintext = cipher.process(cipherTextWithTag);

    return plaintext;
  }

  /// Returns (iv, cipherText, tag)
  static (Uint8List, Uint8List, Uint8List) parseInput(Uint8List encryptedData) {
    // The length of the AES ciphertext is what remains if we subtract the length of iv and tag.
    final tagSize = macSize ~/ 8;
    final cipherTextSize =
        encryptedData.length - aesKeySize /*iv*/ - tagSize /*tag*/;
    final parts =
        _splitAt(encryptedData, [aesKeySize, cipherTextSize, tagSize]);

    return (parts[0], parts[1], parts[2]);
  }

// Split a byte sequence in parts of the specified lengths.
  static List<Uint8List> _splitAt(Uint8List input, List<int> lengths) {
    List<Uint8List> output = List.empty(growable: true);
    var rest = input;

    for (var length in lengths) {
      output.add(rest.sublist(0, length));
      rest = rest.sublist(length);
    }

    return output;
  }

  static Uint8List combineOutput(
      Uint8List iv, Uint8List cipherText, Uint8List tag) {
    final output = Uint8List(iv.length + cipherText.length + tag.length);
    output.setRange(0, iv.length, iv);
    output.setRange(iv.length, iv.length + cipherText.length, cipherText);
    output.setRange(iv.length + cipherText.length, output.length, tag);
    return output;
  }
}
