// ignore_for_file: deprecated_member_use_from_same_package

import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart' as utils;
import 'package:dart_connect/src/crypto/algorithm_names.dart';
import 'package:dart_connect/src/crypto/curve.dart';
import 'package:dart_connect/src/crypto/secure_random.dart';
import 'package:dart_connect/src/crypto/ec.dart';
import 'package:pointycastle/digests/sha256.dart';
import 'package:pointycastle/key_derivators/api.dart';

final int hmacSize = SHA256Digest().digestSize;
const int aesKeySize = 128 ~/ 8; // We use AES-128

final utils.ECDomainParameters ecDomain =
    utils.ECDomainParameters('prime256v1');
// The ephemeral public key is encoded with uncompressed SEC1, which schematically is
// 0x04 + bytesOfXcoordinate + bytesOfYcoordinate.
// NB ecDomain.curve.fieldSize is in bits; we want bytes.
final int publicKeySize = ecDomain.curve.fieldSize ~/ 8 * 2 + 1;

class Ecies {
  static Uint8List encrypt(
    PublicKey publicKey,
    Uint8List plainText, {
    Uint8List? sharedInfo1,
    Uint8List? sharedInfo2,
  }) {
    // Input validation
    if (![Curve.secp256r1.value, Curve.prime256v1.value]
        .contains(publicKey.curve)) {
      // NB the two identifiers above both refer to the same curve
      throw Exception('unsupported curve, use secp256r1/prime256v1');
    }
    if (publicKey.isInfinity) {
      throw Exception(
          'invalid public key: Q must be set and unequal to infinity');
    }

    // Create ephemeral key pair
    final KeyPair keyPair = Ec.generateKeyPair(curve: Curve.prime256v1);
    final ephemeralPrivateKey = ecPrivateKeyFromPrivateKey(keyPair.privateKey);
    final ephemeralPublicKey = ecPublicKeyFromPublicKey(keyPair.publicKey);

    final utils.ECPublicKey ecPublicKey = ecPublicKeyFromPublicKey(publicKey);
    // Compute shared secret using ECDH, and derive keys for AES and HMAC using KDF
    final (aesKey, macKey) =
        _deriveKeys(ecPublicKey, ephemeralPrivateKey, sharedInfo1);

    // Encrypt using AES-256 CTR
    final iv = newSecureRandom().nextBytes(aesKeySize);
    final aesParams = utils.ParametersWithIV(utils.KeyParameter(aesKey), iv);
    final cipherText = (utils.StreamCipher(AlgorithmNames.aesCtr.value)
          ..init(true, aesParams))
        .process(plainText);
    final ivAndCipherText = Uint8List.fromList(iv + cipherText);

    // Compute HMAC tag
    final hmacInput =
        Uint8List.fromList(ivAndCipherText + (sharedInfo2 ?? Uint8List(0)));
    final tag = (utils.Mac(AlgorithmNames.sha256Hmac.value)
          ..init(utils.KeyParameter(macKey)))
        .process(hmacInput);

    // Join everything and return
    final encodedEphemeralPublicKey = ephemeralPublicKey.Q!.getEncoded(false);
    return Uint8List.fromList(
        encodedEphemeralPublicKey + ivAndCipherText + tag);
  }

  @Deprecated("Private key will be removed as parameter")
  static Uint8List decrypt(
    PrivateKey privateKey,
    Uint8List cipherTextBytes, {
    Uint8List? sharedInfo1,
    Uint8List? sharedInfo2,
  }) {
    // Input validation
    if (![Curve.secp256r1.value, Curve.prime256v1.value]
        .contains(privateKey.curve)) {
      throw Exception('unsupported curve, use secp256r1/prime256v1');
    }
    if (privateKey.d == null) {
      throw Exception('invalid private key: missing d');
    }
    if (privateKey.d == BigInt.zero || privateKey.d == BigInt.one) {
      throw Exception('invalid private key: d cannot be zero or one');
    }
    final minLength = publicKeySize + aesKeySize /*iv*/ + hmacSize /*tag*/;
    if (cipherTextBytes.length <= minLength) {
      throw Exception(
          'ciphertext too short, must be more than $minLength bytes');
    }
    if (cipherTextBytes.first != 0x04) {
      // We expect the uncompressed SEC1 encoding of the ephemeral public key which always starts with 0x04.
      throw Exception('ephemeral key in unexpected format');
    }

    final (ephemeralPublicKey, iv, cipherText, theirTag) =
        parseInput(cipherTextBytes);

    final utils.ECPrivateKey ecPrivateKey =
        ecPrivateKeyFromPrivateKey(privateKey);
    // Compute shared secret using ECDH, and derive keys for AES and HMAC using KDF
    final (aesKey, macKey) =
        _deriveKeys(ephemeralPublicKey, ecPrivateKey, sharedInfo1);

    // Compute HMAC tag and verify it equals the one on the ciphertext
    final hmacInput =
        Uint8List.fromList(iv + cipherText + (sharedInfo2 ?? Uint8List(0)));
    final ourTag = (utils.Mac(AlgorithmNames.sha256Hmac.value)
          ..init(utils.KeyParameter(macKey)))
        .process(hmacInput);
    if (!constTimeCompare(ourTag, theirTag)) {
      throw Exception('MAC validation on ciphertext failed');
    }

    // Decrypt using AES CTR
    final aesParams = utils.ParametersWithIV(utils.KeyParameter(aesKey), iv);
    return (utils.StreamCipher(AlgorithmNames.aesCtr.value)
          ..init(false, aesParams))
        .process(cipherText);
  }

  /// Returns (ephemeralPublicKey, iv, cipherText, tag)
  static (utils.ECPublicKey, Uint8List, Uint8List, Uint8List) parseInput(
      Uint8List encryptedData) {
    // The length of the AES ciphertext is what remains if we subtract all fixed-length parts.
    final ciphertextSize = encryptedData.length -
        publicKeySize -
        aesKeySize /*iv*/ -
        hmacSize /*tag*/;

    final parts = _splitAt(
        encryptedData, [publicKeySize, aesKeySize, ciphertextSize, hmacSize]);

    final ephemeralPublicKey =
        utils.ECPublicKey(ecDomain.curve.decodePoint(parts[0]), ecDomain);

    return (ephemeralPublicKey, parts[1], parts[2], parts[3]);
  }

  static Uint8List combineOutput(
    utils.ECPublicKey ephemeralPublicKey,
    Uint8List iv,
    Uint8List cipherText,
    Uint8List tag,
  ) {
    final encodedEphemeralPublicKey = ephemeralPublicKey.Q!.getEncoded(false);
    return Uint8List.fromList(
        encodedEphemeralPublicKey + iv + cipherText + tag);
  }

// Perform ECDH om the specified keys to compute a shared secret,
// and from that derive AES and HMAC keys using the Concat KDF.
  static (Uint8List, Uint8List) _deriveKeys(utils.ECPublicKey publicKey,
      utils.ECPrivateKey privateKey, Uint8List? sharedInfo1) {
    final z = (publicKey.Q! * privateKey.d)!;

    // In the version of ECIES we implement, only the x-coordinate of the Diffie-Hellman shared secret is used.
    final xBytes = z.x!.toBigInteger()!.toBytes();

    // Ensure the KDF input is always of the right size by padding 0 bytes on the left if necessary
    final xPadded =
        List<int>.filled(ecDomain.curve.fieldSize ~/ 8 - xBytes.length, 0) +
            xBytes;

    final output =
        concatKDF(Uint8List.fromList(xPadded), 2 * aesKeySize, sharedInfo1);
    return (
      output.sublist(0, aesKeySize),
      SHA256Digest().process(output.sublist(aesKeySize))
    );
  }

// NIST SP 800-56 Concatenation Key Derivation Function (see section 5.8.1).
  static Uint8List concatKDF(Uint8List input, int len, Uint8List? sharedInfo) {
    final output = Uint8List(len);
    (utils.KeyDerivator(AlgorithmNames.sha256ConcatKdf.value)
          ..init(HkdfParameters(input, len * 8)))
        .deriveKey(sharedInfo ?? Uint8List(0), 0, output, 0);
    return output;
  }

  static bool constTimeCompare(Uint8List left, Uint8List right) {
    if (left.length != right.length) {
      return false;
    }

    var r = 0;
    for (int i = 0; i < left.length; i++) {
      r |= left[i] ^ right[i];
    }
    return r == 0;
  }

// Split a byte sequence in parts of the specified lengths.
  static List<Uint8List> _splitAt(Uint8List input, List<int> lengths) {
    List<Uint8List> output = List.empty(growable: true);
    var rest = input;

    for (var length in lengths) {
      output.add(rest.sublist(0, length));
      rest = rest.sublist(length);
    }

    return output;
  }
}

// This is apparently not part of the Dart standard library.
extension ToBytes on BigInt {
  /// Convert the number to big-endian bytes.
  Uint8List toBytes() {
    if (isNegative) {
      throw Exception("can't convert negative number to bytes");
    }

    // >> 3 effictively divides by 8, and adding 7 rounds up instead of down
    int bytes = (bitLength + 7) >> 3;

    final b256 = BigInt.from(256);
    final result = Uint8List(bytes);
    var number = this;
    for (int i = 0; i < bytes; i++) {
      result[bytes - 1 - i] = number.remainder(b256).toInt();
      number = number >> 8;
    }

    return result;
  }
}
