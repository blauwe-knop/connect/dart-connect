import 'dart:convert';
import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart';
import 'package:dart_connect/src/crypto/algorithm_names.dart';
import 'package:dart_connect/src/crypto/curve.dart';
import 'package:dart_connect/src/crypto/secure_random.dart';
import 'package:pointycastle/key_generators/api.dart' as api;
import 'package:pointycastle/ecc/ecc_fp.dart' as ecc_fp;

class PublicKey {
  final String curve;
  final BigInt? x;
  final BigInt? y;
  bool get isInfinity => x == null && y == null;

  PublicKey({required this.curve, required this.x, required this.y});
}

class PrivateKey {
  final String? curve;
  final BigInt? d;

  PrivateKey({required this.curve, required this.d});
}

class KeyPair {
  final PublicKey publicKey;
  final PrivateKey privateKey;

  KeyPair(this.publicKey, this.privateKey);
}

class Ec {
  @Deprecated(
      "replaced by generateKeyPairWithId which is moved to hardware_keystore")
  static KeyPair generateKeyPair({required Curve curve}) {
    final ecParameters = ECDomainParameters(curve.value);
    final keypair = (KeyGenerator(AlgorithmNames.ec.value)
          ..init(
            ParametersWithRandom(
              api.ECKeyGeneratorParameters(ecParameters),
              newSecureRandom(),
            ),
          ))
        .generateKeyPair();

    final ECPublicKey publicKey = keypair.publicKey as ECPublicKey;
    final ECPrivateKey privateKey = keypair.privateKey as ECPrivateKey;
    final String publiKeyPem = CryptoUtils.encodeEcPublicKeyToPem(publicKey);
    final String privateKeyPem =
        CryptoUtils.encodeEcPrivateKeyToPem(privateKey);

    return KeyPair(
      parsePublicKeyFromPem(utf8.encode(publiKeyPem)),
      parsePrivateKeyFromPem(
        utf8.encode(privateKeyPem),
      ),
    );
  }

  static void verifyEcPublicKey(PublicKey publicKey) {
    if (![Curve.secp256r1.value, Curve.prime256v1.value]
        .contains(publicKey.curve)) {
      // NB the two identifiers above both refer to the same curve
      throw Exception('unsupported curve, use secp256r1/prime256v1');
    }
    if (publicKey.isInfinity) {
      throw Exception(
          'invalid public key: x and y must be set and unequal to infinity');
    }
  }

  @Deprecated("Private keys are stored in HW. No longer possible to verify")
  static void verifyEcPrivateKey(PrivateKey privateKey) {
    // Input validation
    if (![Curve.secp256r1.value, Curve.prime256v1.value]
        .contains(privateKey.curve)) {
      throw Exception('unsupported curve, use secp256r1/prime256v1');
    }
    if (privateKey.d == null) {
      throw Exception('invalid private key: missing d');
    }
    if (privateKey.d == BigInt.zero || privateKey.d == BigInt.one) {
      throw Exception('invalid private key: d cannot be zero or one');
    }
  }

  static PublicKey parsePublicKeyFromPem(Uint8List publicKeyPemBytes) {
    String publicKeyPem = utf8.decode(publicKeyPemBytes);
    final ECPublicKey ecPublicKey =
        CryptoUtils.ecPublicKeyFromPem(publicKeyPem);
    final PublicKey publicKey = PublicKey(
      curve: Curve.prime256v1.value,
      x: ecPublicKey.Q?.x?.toBigInteger(),
      y: ecPublicKey.Q?.y?.toBigInteger(),
    );

    return publicKey;
  }

  @Deprecated("Private key should not be put in PEM")
  static PrivateKey parsePrivateKeyFromPem(Uint8List privateKeyPemBytes) {
    String privateKeyPem = utf8.decode(privateKeyPemBytes);
    final ECPrivateKey ecPrivateKey =
        CryptoUtils.ecPrivateKeyFromPem(privateKeyPem);
    final PrivateKey privateKey = PrivateKey(
      curve: ecPrivateKey.parameters?.domainName,
      d: ecPrivateKey.d,
    );

    return privateKey;
  }

  static String parsePublicKeyToPem(PublicKey publicKey) {
    final ECPublicKey ecPublicKey = ecPublicKeyFromPublicKey(publicKey);
    return CryptoUtils.encodeEcPublicKeyToPem(ecPublicKey);
  }

  @Deprecated("Private keys should not be put in PEM")
  static String parsePrivateKeyToPem(PrivateKey privateKey) {
    final ECPrivateKey ecPrivateKey = ecPrivateKeyFromPrivateKey(privateKey);
    return CryptoUtils.encodeEcPrivateKeyToPem(ecPrivateKey);
  }
}

ECPublicKey ecPublicKeyFromPublicKey(PublicKey customPublicKey) {
  if (customPublicKey.x == null || customPublicKey.x == null) {
    throw Exception('x and y cannot be null');
  }
  var params = ECDomainParameters(customPublicKey.curve);
  var pubKey = ECPublicKey(
      ecc_fp.ECPoint(
          params.curve as ecc_fp.ECCurve,
          params.curve.fromBigInteger(customPublicKey.x!)
              as ecc_fp.ECFieldElement?,
          params.curve.fromBigInteger(customPublicKey.y!)
              as ecc_fp.ECFieldElement?,
          false),
      params);

  return pubKey;
}

@Deprecated("private keys should not leave HW keystore")
ECPrivateKey ecPrivateKeyFromPrivateKey(PrivateKey customPrivateKey) {
  if (customPrivateKey.curve == null) {
    throw Exception('invalid private key: curve');
  }
  return ECPrivateKey(
    customPrivateKey.d,
    ECDomainParameters(customPrivateKey.curve!),
  );
}

/// Decode a big integer with arbitrary sign.
/// When:
/// sign == 0: Zero regardless of magnitude
/// sign < 0: Negative
/// sign > 0: Positive
BigInt decodeBigIntWithSign(int sign, List<int> magnitude) {
  if (sign == 0) {
    return BigInt.zero;
  }

  BigInt result;

  if (magnitude.length == 1) {
    result = BigInt.from(magnitude[0]);
  } else {
    result = BigInt.from(0);
    for (var i = 0; i < magnitude.length; i++) {
      var item = magnitude[magnitude.length - i - 1];
      result |= BigInt.from(item) << (8 * i);
    }
  }

  if (result != BigInt.zero) {
    if (sign < 0) {
      result = result.toSigned(result.bitLength);
    } else {
      result = result.toUnsigned(result.bitLength);
    }
  }
  return result;
}
