enum Curve {
  secp256r1('secp256r1'),
  prime256v1('prime256v1');

  const Curve(this.value);
  final String value;
}
